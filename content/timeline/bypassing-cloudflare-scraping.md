---
title: "Bypassing Cloudflare while Web-Scraping via .NET"
date: 2021-05-23T21:05:10+02:00
draft: true
eventname: Web-Scraping via Puppeteer
eventlocation: against Cloudflare-Protection
---

![Cloudflare Logo](https://upload.wikimedia.org/wikipedia/de/a/a2/Cloudflare_logo.svg)

First, a quick disclaimer: While I don't have extended know-how on how [Cloudflare](https://www.cloudflare.com)-protection works for websites, I do have some solid grasp of .NET as a platform. Take out of this what you will...

## Preamble

For a recent project of mine, I wanted to develop a web-scraper, which could provide me with information about certain IP-addresses. As I was not willing to set-up a database of DNS-records myself, I turned to web-scraping. A case could now be made, that this is somewhat against the terms and conditions, on which someone can access certain websites, however I don't quite see an issue with connecting to websites via a headless method, so long as it doesn't result in "spam"-like scenarios. Therefore, my end-goal was to create a lightweight way to get data from [WhatIsMyIPAddress](https://whatismyipaddress.com).

## The Beginning

My first attempt on getting data from this website, was via a trusty tool of mine: [Postman](https://www.postman.com). Most people will know this one, as it is a famous tool for debugging REST-based APIs. Sure enough, the first connection which I made to the website, returned me the HTML which I needed. However, on the second attempt, I got an response which came from [Cloudflare](https://www.cloudflare.com). In simple terms, it basically identified me as a headless entity, and therefore likely as a bot. As I was unfamiliar with this issue, I did some quick research. Not long after I had a more sophisticated setup in mind: [Puppeteer-Sharp](https://github.com/hardkoded/puppeteer-sharp) in combination with the [HTMLAgilityPack](https://html-agility-pack.net). After setting up a simple "xUnit-Test", I again saw the same results from my initial tests with [Postman](https://www.postman.com). Sadly, I also saw the same results, which occurred on my second attempt while utilizing basic HTTP-GET-messages. After some more digging, I found an interesting method included in [Puppeteer-Sharp](https://github.com/hardkoded/puppeteer-sharp): `EmulateAsync`. This method later turned out to be the key to solving my issue.

## A simple Solution

As outlined in my last paragraph, `EmulateAsync` came in clutch. To elaborate a bit, this method does the following steps:

- It gets device-properties from a provided parameter, which usually is a device from the static collection `Puppeteer.Devices`.
- The viewport of the headless browser gets set to the resolution of the entered device.
- The user-agent gets set to the corresponding one of the entered device.

One possible implementation of this method, would be the following code:

```csharp
var randomNumberGenerator = new Random();
await toBeScrapeWebPage.EmulateAsync(Puppeteer.Devices.ElementAt(
randomNumberGenerator.Next(0, Puppeteer.Devices.Count - 1)).Value);
```

This snippet basically takes a random device from the mentioned static collection, and executes the elaborated steps. One could however take this one step further:

```csharp
var randomNumberGenerator = new Random();
await toBeScrapeWebPage.SetRequestInterceptionAsync(true);
toBeScrapeWebPage.Request += ToBeScrapeWebPageOnRequest;
await toBeScrapeWebPage.EmulateAsync(Puppeteer.Devices.ElementAt(
randomNumberGenerator.Next(0, Puppeteer.Devices.Count - 1)).Value);

private static void ToBeScrapeWebPageOnRequest(object sender,
    RequestEventArgs e)
{
    if (e.Request.ResourceType == ResourceType.Image ||
        e.Request.ResourceType == ResourceType.Img ||
        e.Request.ResourceType == ResourceType.Media)
    {
        e.Request.AbortAsync();
    }
    else { e.Request.ContinueAsync(); }
}
```

Now, every request to the site gets aborted, if it is of the `image`-type, which makes the request a whole lot faster. And as it turns out, this seems to be enough to discourage [Cloudflare](https://www.cloudflare.com) from getting suspicious about the requests. To validate this claim, I tested this scraper against [WhatIsMyIPAddress](https://whatismyipaddress.com) and [Crunchyroll](https://www.crunchyroll.com/) for a dew days, which did not seem to throw any errors, and therefore proved my theory correct. To process the data from [Puppeteer-Sharp](https://github.com/hardkoded/puppeteer-sharp) further, one could now select, for example, nodes from the content via the XPath-query, built into [HTMLAgilityPack](https://html-agility-pack.net).

## Conclusion

Again, I don't fully know how [Cloudflare](https://www.cloudflare.com) works and I frankly don't care that much. However, if this approach does fail me in the future, I will update this post with my findings. In the meantime, if you, the reader, find an issue with this approach, then I would be happy to hear about it!
